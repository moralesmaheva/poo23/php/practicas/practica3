<?php
$vocales = [
    "a", "e", "i", "o", "u",
];

//procesamiento
//para seleccionar dos vocales aleatorias del array
$vocal1 = mt_rand(0, count($vocales) - 1);
$vocal2 = mt_rand(0, count($vocales) - 1);
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6</title>
</head>

<body>
    <div>
        En numero de elementos de vocales es <?= count($vocales) ?>
    </div>
    <div>
        Aqui muestra una vocal aleatoria de vocales <?= $vocales[$vocal1] ?>
    </div>
    <div>
        Aqui muestra otra vocal aleatoria de vocales <?= $vocales[$vocal2] ?>
    </div>
</body>

</html>