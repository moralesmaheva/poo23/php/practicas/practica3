<?php
//Utilizar funcion count()
$datos = [
    [
        "nombre" => "Eva",
        "edad" => 50,
    ],
    [
        "nombre" => "Jose",
        "edad" => 40,
        "peso" => 80,
    ],
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>

<body>
    <div>
        El numero de registros del array datos es <?= count($datos) ?>
    </div>
    <div>
        El numero de elementos de Eva es <?= count($datos[0]) ?>
    </div>
</body>

</html>