<?php
//introducir datos en array asociativo
$datos = [
    [
        "nombre" => "Eva",
        "edad" => 50,
    ],
    [
        "nombre" => "Jose",
        "edad" => 40,
        "peso" => 80,
    ],
];

//añadimos datos directamente
$datos[2]["nombre"] = "Lorena";
$datos[2]["edad"] = 80;
$datos[2]["altura"] = 175;

array_push($datos, "Luis", 20, 90,);
array_push($datos, "Oscar", 23,);

var_dump($datos);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8</title>
</head>

<body>

</body>

</html>