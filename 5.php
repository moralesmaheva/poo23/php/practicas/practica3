<?php
// mostrar datos de array
$datos = [
    [
        "nombre" => "Eva",
        "edad" => 50,
    ],
    [
        "nombre" => "Jose",
        "edad" => 40,
        "peso" => 80,
    ],
];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>

<body>
    <div>
        El nombre del primer dato es <?= $datos[0]["nombre"] ?>
    </div>
    <div>
        El nombre del segundo dato es <?= $datos[1]["nombre"] ?>
    </div>
    <div>
        El peso es <?= $datos[1]["peso"] ?>
    </div>
</body>

</html>