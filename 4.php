<?php
//leer elementos del array
$numeros = [
    "cero" => 0,
    "uno" => 1,
    "dos" => 2,
    "tres" => 3,
    "cuatro" => 4,
];

$vocales = [
    "a", "e", "i", "o", "u",
];

//añadir elementos al array vocales
array_push($vocales, "á");
//añadir elemento al array numeros
$numeros["cinco"] = 5;
var_dump($vocales, $numeros);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <div>
        El segundo elemento del array numeros es <?= $numeros["uno"] ?>
    </div>
    <div>
        El segundo elemento del array vocales es <?= $vocales[1] ?>
    </div>

</body>

</html>